package com.example.webload;


import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class BrowserActivity extends AppCompatActivity {

    static String urlRepeat = "";
    static int urlRepeatCount = 0;


    Button next, flightMode;
    private Context mContext, context;

    CookieManager cookieManager;

    String url;
    TextView urlCountId;
    WebView webView;
    private String html_;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);

        next = findViewById(R.id.next);
        webView = findViewById(R.id.myWebView);
        urlCountId = findViewById(R.id.urlCountId);
        flightMode = findViewById(R.id.flightModeToggle);

        //setting the count of url crawled
        String countToDisplay = MainActivity.count + "/" + MainActivity.arr.length;
        urlCountId.setText(countToDisplay);

        context = getApplicationContext();
        mContext = getBaseContext();

        //fetching the url from main Activity
        Intent intent = getIntent();
        url = intent.getStringExtra("url");


        webView.getSettings().setJavaScriptEnabled(true);
//This statement is used to enable the execution of JavaScript.

        webView.setVerticalScrollBarEnabled(false);
//This statement hides the Vertical scroll bar and does not remove it.

        webView.setHorizontalScrollBarEnabled(false);
//This statement hides the Horizontal scroll bar and does not remove it.


        webView.addJavascriptInterface(new BrowserActivity.MyJavaScriptInterface(), "HTMLOUT");


        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {

                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url,
                                      Bitmap favicon) {

            }

            public void onPageFinished(WebView view, String url) {
                view.loadUrl("javascript:HTMLOUT.processHTML(document.documentElement.innerHTML);");
                System.out.println("===========>onPageFinished");

            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);


            }
        });


        webView.clearHistory();
        clearCookies(context);
        webView.loadUrl(url);


//
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(BrowserActivity.this, MainActivity.class);
                startActivity(intent1);

            }
        });

        flightMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clearCookies(context);
                toggleFlightMode();

            }
        });

    }


    private class MyJavaScriptInterface {
        @SuppressWarnings("unused")
        @JavascriptInterface
        public void processHTML(final String html) {
            // process the html as needed by the app
            System.out.println("<<<<============>>>>>processHtml");
            html_ = html;

            if (html_.contains("Parse the tracking code from cookies") || html_.equals("<head></head><body></body>")) {
                Toast.makeText(BrowserActivity.this, "Sorry , you are caught", Toast.LENGTH_LONG).show();

//logic for crawling an url maximum 5 times and toggle flight mode
                if (urlRepeat.equals(url)) {
                    if (urlRepeatCount <= 5) {

                        urlRepeatCount++;
                    } else {
                        goToMain();
                    }

                } else {
                    urlRepeat = url;
                    urlRepeatCount = 1;
                }

                if (urlRepeatCount <= 5) {
                    MainActivity.count--;
                    toggleFlightMode();
                    goToMainWithWait(6);
                }

                if (urlRepeatCount == 6) {
                    goToMain();

                }

            }


            //runtime Permissions to be asked to the user.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 27);
            }
            //If runtime permission ia already granted by the user.
            else {
                MyAsyncTask task = new MyAsyncTask();
                task.execute();
            }

        }
    }

    /**
     * AsyncTAsk to do work in background
     */
    public class MyAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
//            System.out.println(html_);
            try {
                writeToFile(html_, mContext);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /**
     * @param html
     * @param context
     * @throws IOException write to external storage ===>DCIM in phone
     *                     edit the location in your phone-
     */
    public void writeToFile(String html, Context context) throws IOException {

        if (!html.contains("Parse the tracking code from cookies") || html.equals("<head></head><body></body>")) {
            File file;
            final File path =
                    Environment.getExternalStoragePublicDirectory
                            (
                                    //Environment.DIRECTORY_PICTURES
                                    Environment.DIRECTORY_DCIM + "/MyAlbums/books/"
                            );
            // Make sure the path directory exists.
            if (!path.exists()) {
                // Make it, if it doesn't exit
                path.mkdirs();
            }
            //filename for the new file


            String fileName[] = url.split("www.linkedin.com/in/");
            file = new File(path, fileName[1] + ".html");


            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(html);

            myOutWriter.close();

            fOut.flush();
            fOut.close();

        }
    }

    /**
     * @param requestCode
     * @param permissions
     * @param grantResults when permission is granted by the user.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case 27:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    MyAsyncTask task = new MyAsyncTask();
                    task.execute();

                } else {
                    Toast.makeText(mContext, "Permission is not granted", Toast.LENGTH_SHORT).show();
                }
        }
    }


    public void clearCookies(Context context) {

        CookieSyncManager.createInstance(context);
        cookieManager = CookieManager.getInstance();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.removeAllCookies(null);
            cookieManager.removeSessionCookie();
        } else {
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
        }

    }

    public void toggleFlightMode() {

        try {
            Process su = Runtime.getRuntime().exec("su");
            DataOutputStream outputStream = new DataOutputStream(su.getOutputStream());

            outputStream.writeBytes("settings put global airplane_mode_on 1 &&am broadcast -a android.intent.action.AIRPLANE_MODE --ez state true\n");
            outputStream.flush();
            outputStream.writeBytes("settings put global airplane_mode_on 0 &&am broadcast -a android.intent.action.AIRPLANE_MODE --ez state false\n");
            outputStream.flush();


            outputStream.writeBytes("exit\n");
            outputStream.flush();
            su.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public void goToMainWithWait(int i) {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent1 = new Intent(BrowserActivity.this, MainActivity.class);
                startActivity(intent1);

            }
        }, i * 1000);

    }

    public void goToMain() {
        Intent intent = new Intent(BrowserActivity.this, MainActivity.class);
        startActivity(intent);
    }
}


