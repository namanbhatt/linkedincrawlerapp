package com.example.webload;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    static int count = 0;
    static String arr[] = new String[]{
            "https://www.linkedin.com/in/zia-hucks-a958708a",
            "https://www.linkedin.com/in/zackstark",
            "https://www.linkedin.com/in/wjrro",
            "https://www.linkedin.com/in/winstonvk",
            "https://www.linkedin.com/in/william-mcnair-9211b437",
            "https://www.linkedin.com/in/whitney-helwig-20563819",
            "https://www.linkedin.com/in/vincent-tran-2b541822",
            "https://www.linkedin.com/in/victor-juri-a5b58b3",
            "https://www.linkedin.com/in/vanessa-portelli-b6b01040",
            "https://www.linkedin.com/in/tyrahartdesigns",
            "https://www.linkedin.com/in/tylerlthomas",
            "https://www.linkedin.com/in/trish-curato-6186763b",
            "https://www.linkedin.com/in/trey-buckingham-57b0539",
            "https://www.linkedin.com/in/tracy-rine-50738557",
            "https://www.linkedin.com/in/tony-pettis-4494b81a",
            "https://www.linkedin.com/in/tony-gerena-64251710",
            "https://www.linkedin.com/in/toni-boldt-7075177b",
            "https://www.linkedin.com/in/tiago-desouza-08395a6",
            "https://www.linkedin.com/in/thomasciccarelli",
            "https://www.linkedin.com/in/thecrystaleffect",
            "https://www.linkedin.com/in/theaperez",
            "https://www.linkedin.com/in/techloversblog",
            "https://www.linkedin.com/in/taylordini",
            "https://www.linkedin.com/in/taylor-thompson-4a459a67",
            "https://www.linkedin.com/in/tanguy-bodivit",
            "https://www.linkedin.com/in/tamelacarter",
            "https://www.linkedin.com/in/tamar-rose-berg",
            "https://www.linkedin.com/in/stevesnider1",
            "https://www.linkedin.com/in/stephanie-locatelli-4ab89618",
            "https://www.linkedin.com/in/southern-barrel-co-apparel-35832447",
            "https://www.linkedin.com/in/soniadcastro",
            "https://www.linkedin.com/in/smmcintyre",
            "https://www.linkedin.com/in/sjcallan",
            "https://www.linkedin.com/in/shweta-mogha-15a89b5/",
            "https://www.linkedin.com/in/shirley-jean-mcgriff-0609a13b",
            "https://www.linkedin.com/in/shayesteh",
            "https://www.linkedin.com/in/sharon-liles-7120bb19",
            "https://www.linkedin.com/in/sharan-biradar-43a14021",
            "https://www.linkedin.com/in/sevak-mkrtchyan-648047160",
            "https://www.linkedin.com/in/sequoiagorham",
            "https://www.linkedin.com/in/sefigrossman",
            "https://www.linkedin.com/in/scottmehner",
            "https://www.linkedin.com/in/scottlindars",
            "https://www.linkedin.com/in/scottleisawitz",
            "https://www.linkedin.com/in/scottkaufmann",
            "https://www.linkedin.com/in/scott-workman-61b2566",
            "https://www.linkedin.com/in/scott-bowman-2a0653173",
            "https://www.linkedin.com/in/sara-heaton-8b7247123",
            "https://www.linkedin.com/in/sanilchandran",
            "https://www.linkedin.com/in/sandraoles",
            "https://www.linkedin.com/in/sandra-parris-1801329a",
            "https://www.linkedin.com/in/sandra-leiva-43a8714b",
            "https://www.linkedin.com/in/samantha-canfield-87325961",
            "https://www.linkedin.com/in/sachinyelapure",
            "https://www.linkedin.com/in/ryanpmorrow",
            "https://www.linkedin.com/in/ryanchichirico",
            "https://www.linkedin.com/in/ryan-thomson-07b3172",
            "https://www.linkedin.com/in/ryan-keomaka-86426262",
            "https://www.linkedin.com/in/rubenclark",
            "https://www.linkedin.com/in/rosemary-trudden-211a3722",
            "https://www.linkedin.com/in/ronna-rubenstein-67b1345b",
            "https://www.linkedin.com/in/rohini-sawant-03352123",
            "https://www.linkedin.com/in/rogerdickeyjr",
            "https://www.linkedin.com/in/rodrigo-chavez-45489048",
            "https://www.linkedin.com/in/rod-felton-322a3694",
            "https://www.linkedin.com/in/rockdestiny",
            "https://www.linkedin.com/in/robertjspangler",
            "https://www.linkedin.com/in/robertdspiering",
            "https://www.linkedin.com/in/robert-bob-larson-mba-88aa7738",
            "https://www.linkedin.com/in/revmovement",
            "https://www.linkedin.com/in/renee-leong-73a8b86",
            "https://www.linkedin.com/in/rebeccakaufmanward",
            "https://www.linkedin.com/in/rcstevens",
            "https://www.linkedin.com/in/ravi-dutta-1864451a4",
            "https://www.linkedin.com/in/racheldelapena",
            "https://www.linkedin.com/in/pitassidesigns",
            "https://www.linkedin.com/in/phlawlessevents",
            "https://www.linkedin.com/in/perry-sessions-24439280",
            "https://www.linkedin.com/in/pbretz",
            "https://www.linkedin.com/in/paullarrow",
            "https://www.linkedin.com/in/paula-mervyn-a7145838",
            "https://www.linkedin.com/in/patrick-white-b296214a",
            "https://www.linkedin.com/in/patrick-stewart-ba6b17145",
            "https://www.linkedin.com/in/patrick-devitt-8b3671a",
            "https://www.linkedin.com/in/patricia-patton-21186b66",
            "https://www.linkedin.com/in/paolo-dolcini-6b73638",
            "https://www.linkedin.com/in/palesa-charlie-rmtlp-satterthwaite-8343b897",
            "https://www.linkedin.com/in/ocipare-key-mckinley-a159ab12",
            "https://www.linkedin.com/in/nicole-toy-caron-43547395",
            "https://www.linkedin.com/in/nicolaiopopol",
            "https://www.linkedin.com/in/nicholasevangelisti",
            "https://www.linkedin.com/in/nekia-whitfield-017b305b",
            "https://www.linkedin.com/in/nefertari-hazziez-1a211bb0",
            "https://www.linkedin.com/in/mwatsondesign",
            "https://www.linkedin.com/in/moriah-piacente-5a8062191",
            "https://www.linkedin.com/in/mollykrusedesign",
            "https://www.linkedin.com/in/molly-hemstreet-8a3b0043",
            "https://www.linkedin.com/in/mmorowitz/de",
            "https://www.linkedin.com/in/miranda-hirsch-bb898836",
            "https://www.linkedin.com/in/mikey-lopez-43a5561a3",
            "https://www.linkedin.com/in/michelle-cappiello-ba155041",
            "https://www.linkedin.com/in/michele-piccolino",
            "https://www.linkedin.com/in/michele-bates-73a88929",
            "https://www.linkedin.com/in/michaeldarragh",
            "https://www.linkedin.com/in/megan-reynolds-6036a76",
            "https://www.linkedin.com/in/matthewgreene1",
            "https://www.linkedin.com/in/matthew-jay-landon-a8518a46",
            "https://www.linkedin.com/in/mary-gottman-86356037",
            "https://www.linkedin.com/in/mary-beth-mcgarr-b47962118",
            "https://www.linkedin.com/in/martinpagh",
            "https://www.linkedin.com/in/markwalzjr",
            "https://www.linkedin.com/in/markdwallace",
            "https://www.linkedin.com/in/mark-oehlert-2020b47",
            "https://www.linkedin.com/in/mark-kroh-6556216",
            "https://www.linkedin.com/in/marina-ross-235798bb",
            "https://www.linkedin.com/in/maria-veres-815478a",
            "https://www.linkedin.com/in/maria-andrea-encalada-85a36ba7",
            "https://www.linkedin.com/in/marc-gowland-a95a91",
            "https://www.linkedin.com/in/manuel-pacheco-64b8a414",
            "https://www.linkedin.com/in/lynn-jackson-13a0951a4",
            "https://www.linkedin.com/in/lyn-lavery-53ab2017",
            "https://www.linkedin.com/in/luisrherrera",
            "https://www.linkedin.com/in/luisfreitas",
            "https://www.linkedin.com/in/luis-aguilar-593a4940",
            "https://www.linkedin.com/in/lucia-rodriguez-hernandez-92b966138",
            "https://www.linkedin.com/in/lorri-schneider-39516815",
            "https://www.linkedin.com/in/lisa-strickler-11a1b5107",
            "https://www.linkedin.com/in/lisa-calixto-b5a42a56",
            "https://www.linkedin.com/in/lindsay-lavell-a71ab3104",
            "https://www.linkedin.com/in/lily-shafroth-41a4293b",
            "https://www.linkedin.com/in/lilian-mcdonnell-43448a43",
            "https://www.linkedin.com/in/laurie-brown-316a907",
            "https://www.linkedin.com/in/laurel-laurel-debert-158ab973",
            "https://www.linkedin.com/in/laura-sandler-7a7b55ab",
            "https://www.linkedin.com/in/larrywilliamson",
            "https://www.linkedin.com/in/larry-stewart-762366104",
            "https://www.linkedin.com/in/lara-sloma-b3261663",
            "https://www.linkedin.com/in/lanita-w-70579935",
            "https://www.linkedin.com/in/lachel-dreyer-83831990",
            "https://www.linkedin.com/in/kyle-graven-ab760561",
            "https://www.linkedin.com/in/kristineeckart",
            "https://www.linkedin.com/in/kristina-miele-58a375120",
            "https://www.linkedin.com/in/kreamopy",
            "https://www.linkedin.com/in/kimberly-dietrich-2b683052",
            "https://www.linkedin.com/in/kezell",
            "https://www.linkedin.com/in/kerstin-schulz-8b515325",
            "https://www.linkedin.com/in/kenziearne",
            "https://www.linkedin.com/in/ken-sciuto-1b72168",
            "https://www.linkedin.com/in/kelsiemackie",
            "https://www.linkedin.com/in/kelly-sheffler-16483684",
            "https://www.linkedin.com/in/kellieann-williams-1a260414",


    };


    EditText url;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        url = findViewById(R.id.getUrl);
        int urlCount = count + 1;
        url.setText(arr[count] + "With url count " + urlCount);

        if (count <= arr.length) {

            Intent intent = new Intent(MainActivity.this, BrowserActivity.class);
            intent.putExtra("url", arr[count]);
            count++;
            startActivity(intent);
        }


    }
}

